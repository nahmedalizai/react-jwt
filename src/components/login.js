import React, {useState} from 'react';
import {Container, Row, Col, Form, ButtonToolbar, Button} from 'react-bootstrap';

function Login(props) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const onEmailChange = (event) => setEmail(event.target.value);
    const onPasswordChange = (event) => setPassword(event.target.value);

    return <Container>
    <Row>
      <Col md={{ span: 6, offset: 3 }}>
        <form className="form" onSubmit={(event) => props.handleLoginSubmit(event,email, password)}>
          <label>Email address</label>
          {/* <Form.Control type="email" placeholder="Enter email" required={true} onChange={(event) => onEmailChange(event)} /> */}
          <Form.Control type="text" placeholder="Enter email" required={true} onChange={(event) => onEmailChange(event)} />
          <label>Password</label>
          <Form.Control type="password" placeholder="Password" required={true} onChange={(event) => onPasswordChange(event)} />
          <ButtonToolbar style={{ marginTop: "10px" }}>
            <Button variant="primary" type="submit" size="md" block > Submit </Button>
          </ButtonToolbar>
        </form>
      </Col>
    </Row>
  </Container>;

}

export default Login;