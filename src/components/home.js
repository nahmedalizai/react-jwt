import React, { useState } from 'react';
import Axios from 'axios';
import { ButtonToolbar, Row, Col, Container, Button } from 'react-bootstrap';

function Home(props) {
    const [orders, setOrders] = useState();

    const handleListOrders = (event) => {
        Axios({
            method: 'GET',
            url: 'http://localhost:3300/orders',
            headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
        })
        .then((response) => {
            if (response && response.status === 200) {
                console.log(response.data);
                setOrders(response.data);
            }
        })
        .catch((error) => { console.log(error) })
        event.preventDefault();
    }

    return <div>
        <Container>
            <Row>
                <Col md={{ span: 6, offset: 3 }}>
                    <ButtonToolbar style={{ marginTop: "10px" }}>
                        <Button variant="primary" type="submit" size="md" block onClick={(event) => handleListOrders(event)} > List Orders </Button>
                    </ButtonToolbar>
                </Col>
            </Row>
        </Container>
    </div>
}

export default Home;