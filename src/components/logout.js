import React from 'react';
import { ButtonToolbar, Row, Col, Container, Button } from 'react-bootstrap';

function Logout(props) {
    return <div>
        <Container>
            <Row>
                <Col md={{ span: 6, offset: 3 }}>
                    <ButtonToolbar style={{ marginTop: "10px" }}>
                        <Button variant="primary" size="md" block onClick={props.onLogout}> Logout </Button>
                    </ButtonToolbar>
                </Col>
            </Row>
        </Container>
    </div>;
}

export default Logout;