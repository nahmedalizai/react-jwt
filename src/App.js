import React, {useState, useEffect} from 'react';
import './App.css';
import Axios from 'axios';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Home from './components/home';
import Login from './components/login';
import Logout from './components/logout';

function App() {
  const [login, setLogin] = useState({
    isAuthorized: false,
    token: ''
  });

  useEffect(() => {
    if(localStorage.getItem('isAuthorized')){
      console.log('called on load');
      setLogin({
        isAuthorized : true,
        token : localStorage.getItem('token')
      });
    }
  },[]);

  const handleLogin = (event, email, password) => {
    Axios({
      method: 'POST',
      url: 'http://localhost:3300/users/login/',
      data: {
        email: email,
        password: password
      },
      headers: {
          'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        if (response && response.status === 200) {
          localStorage.setItem('isAuthorized', true);
          localStorage.setItem('token', response.data.token);
          setLogin({
            isAuthorized : true,
            token : response.data.token
          });
        }
        else
          localStorage.clear();
      })
      .catch((error) => { console.log(error) })
    event.preventDefault();
  }

  const handleLogout = () => {
    localStorage.removeItem('isAuthorized');
    localStorage.removeItem('token');
    setLogin({
      isAuthorized : false,
      token : ''
    });
  }

  return (
    <div className="App">
      {!login.isAuthorized && <Login handleLoginSubmit={handleLogin} />}
      {login.isAuthorized && <Logout onLogout={handleLogout}/>}
      {login.isAuthorized && <Home />}
    </div>
  );
}

export default App;
